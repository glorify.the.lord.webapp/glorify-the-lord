/**
 * Models the data of a quote (without id)
 */
 export default class QuoteData {
      
  /**
   * Content of the quote
   * 
   * @type {string}
   */
  content;
  
  /**
   * Author of the quote
   * 
   * @type {string}
   */
  author;
  
  /**
   * Instantiates a QuoteData object with the given content and author
   * 
   * @param {string} content of the instantiated QuoteData
   * @param {string} author  of the instantiated QuoteData
   */
  constructor(content, author) {
    this.content = content;
    this.author = author;
  }

  /**
   * Creates a QuoteData object with the data found in dataObject.
   * For every property in dataObject whose name matches the name a field of 
   * the QuoteData class the value of that property is used as value for that 
   * field.
   * 
   * @param {object} dataObject 
   * @returns QuoteData with data provided by dataObject or null / undefined if
   * object is null / undefined
   */
   static createFromDataObject(dataObject) {
    if (dataObject == null) {
      return dataObject
    }

    return new QuoteData(
      dataObject.content,
      dataObject.author
    )
  }
}