/**
 * This module allows to control the Waiting-Notification.
 * The Waiting-Notification is a toast / snackbar which informs the user that
 * he has to wait, because a connection to the server is still being
 * established.
 */
import { Notify, QSpinnerGears } from "quasar";

/**
 * Configuration options used for the Waiting-Notification.
 *
 * @type QNotifyCreateOptions
 */
const waitingNotificationOptions = {
  spinner: QSpinnerGears, // fancy spinner animation
  message: "App will be back soon",
  caption: "Connecting to Server ...",
  position: "center",
  color: "primary",
  timeout: 0, // infinite timeout
};

/**
 * This variable tracks whether the Waiting-Notification is shown or not.
 *
 * @type boolean
 */
let isNotificationShown = false;

/**
 * This variable contains a function to close / dismiss the Waiting-Notification.
 *
 * @type Function<() => void>
 */
let dismissNotification;


/**
 * Ensures that the Waiting-Notification is shown.
 */
export function ensureNotificationIsShown() {
  if (!isNotificationShown) {
    dismissNotification = Notify.create(waitingNotificationOptions);
    isNotificationShown = true;
  }
}

/**
 * Ensures that the Waiting-Notification is not shown.
 */
export function ensureNotificationIsNotShown() {
  if (isNotificationShown) {
    dismissNotification();
    isNotificationShown = false;
  }
}