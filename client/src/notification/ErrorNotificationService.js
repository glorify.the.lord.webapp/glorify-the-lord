/**
 * This module allows to display Error-Notifications.
 * An Error-Notification is a toast / snackbar which displays an error message.
 */
import { Notify } from "quasar";

Notify.registerType(
  "error",
  {
    position: "bottom",
    color: "red",
    timeout: 3000,
    icon: "error",
  }
)

/**
 * Shows an Error-Notification with the given message.
 * 
 * @param {string} errorMessage message displayed by the Error-Notification.
 */
export default function showErrorNotification(errorMessage) {
  Notify.create(
    {
      type: "error",
      message: errorMessage  
    }
  )
}