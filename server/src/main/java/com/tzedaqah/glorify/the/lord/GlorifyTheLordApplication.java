package com.tzedaqah.glorify.the.lord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlorifyTheLordApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlorifyTheLordApplication.class, args);
	}

}
