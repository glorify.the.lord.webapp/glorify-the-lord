package com.tzedaqah.glorify.the.lord.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;


/**
 * A <code>Quote</code> represents a quote form a person.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Quote {

    /**
     * Id
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Version used for optimistic locking.
     */
    @Version
    private Integer version;

    /**
     * The content of the quote is what the quote says.
     */
    private String content;

    /**
     * The author of the quote is who said the quote.
     */
    private String author;

    /**
     * The source of the quote says where to find the quote, e.g. a link.
     */
    private String source;

}
