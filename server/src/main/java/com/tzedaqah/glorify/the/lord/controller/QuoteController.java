package com.tzedaqah.glorify.the.lord.controller;

import com.tzedaqah.glorify.the.lord.model.Quote;
import com.tzedaqah.glorify.the.lord.model.dto.QuoteDataDTO;
import com.tzedaqah.glorify.the.lord.service.QuoteService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for interacting with {@link Quote Quotes}.
 */
@RestController()
@AllArgsConstructor
@RequestMapping("/api/1.1/quote")
public class QuoteController {

    /**
     * Service used for performing tasks with {@link Quote Quotes}.
     */
    @Autowired
    private final QuoteService quoteService;

    /**
     * @return all <code>Quote</code>s available
     */
    @GetMapping("all")
    public List<Quote> getAll() {
        return quoteService.getAll();
    }

    /**
     * @return the <code>Quote</code> whose {@link Quote#getId() id} is <code>id</code> or null if no such
     * <code>Quote</code> exists
     */
    @GetMapping("{id}")
    public Quote get(@PathVariable long id) {
        return quoteService.getById(id);
    }

    /**
     * Adds new <code>Quote</code> with the data given by <code>dataOfNewQuote</code>.
     *
     * @param dataOfNewQuote data of the new <code>Quote</code>
     * @return the newly created <code>Quote</code>
     */
    @PostMapping(
            value = "new",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote addNew(@RequestBody QuoteDataDTO dataOfNewQuote) {
        return quoteService.addQuoteWithContentAndAuthor(
                dataOfNewQuote.getContent(),
                dataOfNewQuote.getAuthor()
        );
    }

    /**
     * Removes the <code>Quote</code> with the {@link Quote#getId() id} given by <code>id</code>.
     *
     * @param id non-null {@link Quote#getId() id} of the <code>Quote</code> to be removed
     */
    @DeleteMapping("{id}")
    public void remove(@PathVariable long id) {
        quoteService.remove(id);
    }

    /**
     * Updates the data of the <code>Quote</code> whose {@link Quote#getId() id} is
     * <code>id</code> with the data in <code>newDataOfQuote</code>.
     * Expects <code>id</code> to be the {@link Quote#getId() id} of an existing <code>Quote</code>.
     *
     * @param id             {@link Quote#getId() id} of an existing <code>Quote</code>
     * @param newDataOfQuote new data of the <code>Quote</code> with {@link Quote#getId() id} <code>quoteId</code>
     * @return the updated <code>Quote</code>
     * @throws IllegalArgumentException if no <code>Quote</code> with the {@link Quote#getId() id} <code>quoteId</code>
     *                                  exists
     */
    @PostMapping(
            value = "update/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Quote updateContent(@PathVariable long id, @RequestBody QuoteDataDTO newDataOfQuote) {
        return quoteService.updateContentAndAuthorOfQuote(
                id,
                newDataOfQuote.getContent(),
                newDataOfQuote.getAuthor()
        );
    }
}
