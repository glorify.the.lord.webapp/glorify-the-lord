package com.tzedaqah.glorify.the.lord.configuration;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.yaml.snakeyaml.util.ArrayUtils;

import static java.util.Collections.singletonList;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@SuppressWarnings({"unused", "SpellCheckingInspection"})
@Configuration
@ConfigurationProperties(prefix = "cors")
@Setter
public class SecurityConfiguration {

    /**
     * List of origins (urls) that are allowed to use the api provided by this app.
     */
    private String[] allowedOrigins;

    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // *** URL below needs to match the Vue client URL and port ***
        config.setAllowedOriginPatterns(
                ArrayUtils.toUnmodifiableList(allowedOrigins)
        );
        config.setAllowedMethods(
                singletonList("*")
        );
        config.setAllowedHeaders(
                singletonList("*")
        );

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        var bean = new FilterRegistrationBean<>(
                new CorsFilter(source)
        );
        bean.setOrder(HIGHEST_PRECEDENCE);
        return bean;
    }
}
