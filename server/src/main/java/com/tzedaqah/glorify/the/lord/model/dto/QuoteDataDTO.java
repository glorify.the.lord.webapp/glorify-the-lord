package com.tzedaqah.glorify.the.lord.model.dto;

import com.tzedaqah.glorify.the.lord.model.Quote;
import lombok.Builder;
import lombok.Getter;

/**
 * This object transports data about a {@link Quote}, which is called the associated <code>Quote</code>.
 */
@Builder
@Getter
public class QuoteDataDTO {

    /**
     * Contains the {@link Quote#getContent() content} of the associated {@link Quote}.
     */
    private final String content;

    /**
     * Contains the {@link Quote#getAuthor() author} of the associated {@link Quote}.
     */
    private final String author;
}
